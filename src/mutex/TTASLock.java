package mutex;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class TTASLock implements Lock {
	private AtomicBoolean state = new AtomicBoolean(false);
	private Boolean isBackOff = false;
	private int minDelay = 0;
	private int maxDelay = 0;
	private int limit = 0;
	private Random random = new Random();
	
	public TTASLock() {}
	public TTASLock(int meanDelay, Boolean isBackOff, int minDelay, int maxDelay) {
		this.isBackOff = isBackOff;
		if (maxDelay > 0) {
			this.minDelay = minDelay;
			this.maxDelay = maxDelay;
		}
		else {
			this.minDelay = meanDelay / 5;
			this.maxDelay = meanDelay * 2;
		}
		this.limit = this.minDelay;
	}
	
	void backoff() {
		int delay = random.nextInt(limit + 1);
		limit = Math.min(maxDelay, 2 * limit);
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void lock() {
		while (true) {
			while (state.get()) {}
			if (!state.getAndSet(true)) {
				return;
			}
			else if (isBackOff) {
				backoff();
			}
		}
	}

	@Override
	public void lockInterruptibly() throws InterruptedException {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean tryLock() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tryLock(long time, TimeUnit unit)
			throws InterruptedException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void unlock() {
		state.set(false);
	}

	@Override
	public Condition newCondition() {
		// TODO Auto-generated method stub
		return null;
	}

}
