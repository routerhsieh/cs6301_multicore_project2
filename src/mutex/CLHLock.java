package mutex;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class CLHLock implements Lock {
	private final AtomicReference<QNode> tail = new AtomicReference<QNode>(new QNode());
	private final ThreadLocal<QNode> myPred;
	private final ThreadLocal<QNode> myNode;
	
	public CLHLock() {
		this.myNode = new ThreadLocal<QNode>() {
			protected QNode initialValue() {
				return new QNode();
			} 
		};
		this.myPred = new ThreadLocal<QNode>() {
			protected QNode initialValue() {
				return null;
			}
		};
	}
	
	private static class QNode {
		private volatile boolean locked;
	}
	
	@Override
	public void lock() {
		final QNode qnode = this.myNode.get();
		qnode.locked = true;
		QNode pred = tail.getAndSet(qnode);
		this.myPred.set(pred);
		while (pred.locked) {}
	}

	@Override
	public void lockInterruptibly() throws InterruptedException {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean tryLock() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tryLock(long time, TimeUnit unit)
			throws InterruptedException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void unlock() {
		final QNode qnode = this.myNode.get();
		qnode.locked = false;
		this.myNode.set(this.myPred.get());
	}

	@Override
	public Condition newCondition() {
		// TODO Auto-generated method stub
		return null;
	}

}
