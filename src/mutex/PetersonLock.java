package mutex;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class PetersonLock {
	private final AtomicInteger nextId = new AtomicInteger(0);
	private final ThreadLocal<Integer> ThreadId = new ThreadLocal<Integer>() {
		protected Integer initialValue() {
			return nextId.getAndIncrement();
		}
	};
	
	private final AtomicBoolean[] flag = new AtomicBoolean[2];
	private volatile int victim;
	
	public PetersonLock () {
		for (int i = 0; i < flag.length; i++) {
			flag[i] = new AtomicBoolean();
		}
	}
	
	public void lock() {
		int i = ThreadId.get() % 2;
		int j = 1 - i;
		flag[i].set(true);
		victim = i;
		while (flag[j].get() && victim == i) {};
	}
	
	public void lock(int id) {
		int i = id % 2;
		int j = 1 - i;
		flag[i].set(true);
		victim = i;
		while (flag[j].get() && victim == i) {};
	}
	
	public void unlock() {
		int i = ThreadId.get() % 2;
		flag[i].set(false);
	}
	
	public void unlock(int id) {
		int i = id % 2;
		flag[i].set(false);
	}
}
