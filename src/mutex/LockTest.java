package mutex;

import java.util.Random;
import java.util.concurrent.locks.Lock;

public class LockTest {
	private static final int requirements = 1000;
	private int threadNumber = Runtime.getRuntime().availableProcessors();
	private long expectedResult = requirements * threadNumber;
	private long computedResult = 0;
	private int meanDelay = 0;
	private double totalTimes;
	private boolean isCorrect = false;
	private Random random = new Random();
	private Lock lock;
	private Thread[] threads;

	public LockTest(int threadNumber, int meanDelay, Lock lock) {
		this.meanDelay = meanDelay;
		this.threadNumber = threadNumber;
		this.expectedResult = threadNumber * requirements;
		this.lock = lock;
		this.threads = new Thread[threadNumber];
	}
	
	protected class IncrementOne extends Thread {
		public void run() {
			for (int i = 0; i < requirements; i++) {
				lock.lock();
				try {
					computedResult += 1;
				} finally {
					lock.unlock();
				}
				
				try {
					Thread.sleep(nextSleep());
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	protected long nextSleep() {
		return (long) Math.log(1 / (1 - random.nextDouble())) * meanDelay;
	}

	protected void test() throws Exception {
		long startTime = System.currentTimeMillis();
		
		for (int i = 0; i < threadNumber; i++) {
			threads[i] = new IncrementOne();
		}
		
		for (int i = 0; i < threadNumber; i++) {
			threads[i].start();
		}
		
		for (int i = 0; i < threadNumber; i++) {
			threads[i].join();
		}
		
		this.isCorrect = (computedResult == expectedResult);
		
		long endTime = System.currentTimeMillis();
		
		this.totalTimes = (double)(endTime - startTime);
	}

	public double getRunningTime() {
		return this.totalTimes;
	}

	public int getTotalRequests() {
		return requirements * threadNumber;
	}

	public boolean isCorrect() {
		return this.isCorrect;
	}

	public long getExpectedResult() {
		return this.expectedResult;
	}

	public long getComputedResult() {
		return this.computedResult;
	}

	public double getThroughput() {
		int totalRequest = requirements * threadNumber;
		
		return totalRequest / totalTimes * 1000;
	}
}
