package mutex;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class PetersonTreeLock implements Lock{
	private final AtomicInteger nextId = new AtomicInteger(0);
	private final ThreadLocal<Integer> ThreadId = new ThreadLocal<Integer>() {
		protected Integer initialValue() {
			return nextId.getAndIncrement();
		}
	};
	private int maxLevel;
	private int threads = 8;
	private ArrayList<ArrayList<PetersonLock>> levelLocks = new ArrayList<ArrayList<PetersonLock>>();

	private void initialTreeLock() {
		this.maxLevel = (int) Math.ceil(Math.log(threads) / Math.log(2));
		for (int level = 0; level < maxLevel; level++) {
			int maxLocks = (int) Math.pow(2, level);
			ArrayList<PetersonLock> currLevel = new ArrayList<PetersonLock>();
			for (int lock = 0; lock < maxLocks; lock++) {
				currLevel.add(new PetersonLock());
			}
			levelLocks.add(currLevel);
		}
	}

	public PetersonTreeLock() {
		initialTreeLock();
	};
	public PetersonTreeLock(int threads) {
		this.threads = threads;
		initialTreeLock();
	};
	
	public void lock() {
		int lockNum = ThreadId.get();
		int level = this.maxLevel - 1;
		int currentId;
		
		while (level >= 0) {
			currentId = lockNum;
			lockNum = (int) Math.floor(lockNum / 2);
			levelLocks.get(level).get(lockNum).lock(currentId);
			level -= 1;
		}
	}
	
	public void unlock() {
		int lockNum = ThreadId.get();
		int level = 0;
		ArrayList<Integer> reversePath = new ArrayList<Integer>();
		
		reversePath.add(lockNum);
		for (int idx = 0; idx < this.maxLevel; idx++) {
			lockNum = (int) Math.floor(lockNum / 2);
			reversePath.add(lockNum);
		}
		
		for (int idx = reversePath.size() - 1; idx > 0; idx--) {
			levelLocks.get(level).get(reversePath.get(idx)).unlock(reversePath.get(idx - 1));
			level += 1;
		}
	}

	@Override
	public void lockInterruptibly() throws InterruptedException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean tryLock() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tryLock(long time, TimeUnit unit)
			throws InterruptedException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Condition newCondition() {
		// TODO Auto-generated method stub
		return null;
	}
}
