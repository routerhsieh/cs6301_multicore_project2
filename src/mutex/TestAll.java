package mutex;

public class TestAll {
	public static void main(String args[]) {
		String testType;
		int threadNumber, delayTime;
		if (args.length < 3) {
			System.out.printf("Please enter test type, number of thread and delay times\n");
			return;
		}
		else {
			testType = args[0];
			threadNumber = Integer.parseInt(args[1]);
			delayTime = Integer.parseInt(args[2]);
		}
		
		int rounds;
		if (args.length >= 4) {
			rounds = Integer.parseInt(args[3]);
		}
		else {
			rounds = 1;
		}
		
		int minDelay = 0, maxDelay = 0;
		if (args.length >= 6) {
			minDelay = Integer.parseInt(args[4]);
			maxDelay = Integer.parseInt(args[5]);
		}
		
		LockTest lockTest = null;
		for (int round = 0; round < rounds; round += 1) {
			try {
				//LockTest lockTest;
				switch (testType) {
				case "peterson":
					lockTest = new LockTest(threadNumber, delayTime, new PetersonTreeLock(threadNumber));
					break;
				case "tas":
					lockTest = new LockTest(threadNumber, delayTime, new TASLock());
					break;
				case "ttas":
					lockTest = new LockTest(threadNumber, delayTime, new TTASLock());
					break;
				case "ttas-eb":
					lockTest = new LockTest(threadNumber, delayTime, new TTASLock(delayTime, true, minDelay, maxDelay));
					break;
				case "clh":
					lockTest = new LockTest(threadNumber, delayTime, new CLHLock());
					break;
				default:
					System.out.printf("%s is unknown type!!\n", testType);
					return;
				}
				lockTest.test();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (lockTest.isCorrect()) {
				System.out.printf("Counter: %d\n", lockTest.getComputedResult());
				System.out.printf("ThreadNumber: %d\n", threadNumber);
				System.out.printf("DelayTime: %d\n", delayTime);
				System.out.printf("Throughput: %f\n", lockTest.getThroughput());
				System.out.println();
			} else {
				System.out.printf("Wrong!! Expected = %d, Computed = %d\n",
						lockTest.getExpectedResult(),
						lockTest.getComputedResult());
			}
		}
	}
}
